(function($) {

	$.fn.extend({
		treeActive: function(selector) {
			return $('.active-tree').parent();
		},
		addEventCustomerCategory: function() {
			console.log('addEventCustomerCategory()');

			$('#tab-view span').click( function( e ) {
				$('.active').removeClass('active');
				$(this).addClass('active');
				if(this.id === 'tab-ACL') {
					$('#body-main-customer-category').hide();
					$('#body-ACL-customer-category').show();
				} else if (this.id === 'tab-main-UDF') {
					$('#body-ACL-customer-category').hide();
					$('#body-main-customer-category').show();
				}
				return false;
			});

			$('#new-customer-category').click( function(e) {
				console.log('new-customer-category');
				$('.titleTab').remove();
				$('#body-customer-category > div').fadeOut(200);
				$('#combobox').remove();
				var loading = $().loading();
				$('#content-customer-category').append(loading);
				setTimeout( function() {
					$.get('customerCategory-new.html', function( data ) {
						loading.remove();
						$('#body-customer-category').append(data).addEventComboBox({
							getUrl: 'link.get/post (new Customer Category)',
							addUserUrl: 'links Add User (new Customer Category)',
							addGroupUrl: 'links add Group (new Customer Category)'
						});
						svgcheck();
					}, 'html');
				}, 800);

				$('#box-header-customer-category').append($('<div class="titleTab activeMenu">New Customer Category</div>'));


				e.stopPropagation();
			});

			$('#edit-customer-category').click( function(e) {
				var tree = document.querySelector('.active-tree');
				if (tree !== null) {
					$('.titleTab').remove();
					$('#body-customer-category > div').fadeOut(200);
					$('#combobox').remove();
					var loading = $().loading();
					$('#content-customer-category').append(loading);
					setTimeout( function() {

						$.get('customerCategory-edit.html', function( data ) {
							loading.remove();
							$("#body-customer-category").append(data).addEventComboBox({
								getUrl: 'link.get/post (new Customer Category)',
								addUserUrl: 'links Add User (new Customer Category)',
								addGroupUrl: 'links add Group (new Customer Category)'
							});
							svgcheck();
						}, 'html');
					}, 800);

					$('#box-header-customer-category').append($('<div class="titleTab activeMenu">Edit Customer Category</div>'));

					console.log('edit-customer-category');
				}
				e.stopPropagation();
			});

			$('#delete-customer-category').click( function(e) {
				var tree = document.querySelector('.active-tree');
				if (tree !== null) {
					var $this = $(this);
					$('body').append('<div id="layoutDeleteTree"><div><p>bạn có muốn Delete Tree này?</p><div id="DeleteTree"><span class="noDelete">No</span><span class="yesDelete">Yes</span></div></div></div>');

					$('#DeleteTree span').click( function( e ) {
						if( $(this).hasClass('yesDelete') ) {
							$('.titleTab').remove();
							$("#treeBody").getActiveNode().parent().remove();
							$('#body-customer-category').html('');
							console.log('viet Layout delete customer category');
						}
						$('#layoutDeleteTree').remove();
						e.stopPropagation();
					});
				}
				e.stopPropagation();
			});
		},
		addEventComboBox: function() {
			var setting = arguments[0] || {};
			var browser = $().browserName();
			console.log(setting);

			if (!(browser === 'Chrome' || browser === 'Safari' || browser === 'Opera')) {
				$(function() {
					$(".date").datepicker();
				});
			};

			$('select:not([name="quyenUser"],[name="quyenGroup"])').msDropdown({roundedBorder:false});
			$('.changeACL').hide();

			
			$('#tab-view-new-edit span').click( function() {
				$('.actived').removeClass('actived');
				$(this).addClass('actived');
				if(this.id === 'tab-ACL') {
					$('.change-Main-Udf').hide();
					$('.changeACL').show();
				} else if (this.id === 'tab-main-UDF') {
					$('.changeACL').hide();
					$('.change-Main-Udf').show();
				}
			});

			$('#exit-combobox').click( function( e ) {
				$('.titleTab').remove();
				$('#combobox').fadeOut(300).remove();
				$('#body-customer-category > div').show();
				$('.quyenUser span').unbind('click');
				$('.quyenGroup span').unbind('click');
				e.stopPropagation();
			});

			$('#save-combobox').click( function( e ) {
				$('#exit-combobox').click();
				var id = $(this).treeActive().data('id');

				console.log(id);

				console.log('viet ham Ajax $.get(', setting.getUrl, ')');
				$.post(setting.getUrl, function( data ) {
					// viet ham post save 
				});

				e.stopPropagation();
			});

			$('.addUser').click( function( e ) {
				var form = document.formAddUser;
				if (form.userAdd.value != '' && form.quyenUser.value != '') {
					console.log('viet ham Ajax add User');
					// $.post(setting.addUserUrl, function(data) {
						
						// neu post add user thanh cong thi them (data) tra ve vao .listUser
						// $('.listUser').append(data);
						$('.listUser').prepend($('<li data-id="'+ form.userAdd.value +'" class="border-bottom-1-2"><div class="nameUser"><a href="#" class="icon-user">'+ form.userAdd.value +'</a></div><div class="quyenUser"><span class="border-1 applyQuyen" data-quyen="read">Read</span><span class="border-1" data-quyen="write">Write</span><span class="border-1"  data-quyen="admin">Admin</span></div><div class="deleteUser"><span class="removeUser icon-close"></span></div></li>').fadeIn(600));
					// });


					console.log('addUser- ', setting.addUserUrl)
				}

				$('.removeUser').unbind('click').bind('click', removeUser);
				$('.quyenUser span').unbind('click').bind('click', quyenUser);
				e.stopPropagation();
			});

			$('.addGroup').click( function( e ) {
				var form = document.formAddGroup;
				if (form.selectGroup.value != '' && form.quyenGroup.value != '') {
					console.log('Viet ham Ajax add Group');
					// $.post(setting.addGroupUrl, function( data ) {

						// neu post add Group thanh cong thi them (data) tra ve vao .listGroup
						// $('.listGroup').append(data);
						$('.listGroup').prepend($('<li data-id="'+ form.selectGroup.value +'" class="border-bottom-1-2"><div class="nameGroup"><span class="icon-users">'+ form.selectGroup.value +'</span></div><div class="quyenGroup"><span class="border-1" data-quyen="read">Read</span><span class="border-1" data-quyen="write">Write</span><span class="border-1 applyQuyen" data-quyen="admin">Admin</span></div><div class="deleteGroup"><span class="removeGroup icon-close"></span></div></li>').fadeIn(600));
					// });
					console.log('addGroup- ', setting.addGroupUrl)
				}

				$('.removeGroup').unbind('click').bind('click', removeGroup);
				$('.quyenGroup span').unbind('click').bind('click', quyenGroup);
				e.stopPropagation();
			});

			$('.removeUser').click(removeUser);
			function removeUser( e ) {
				var parent = $(this).parent().parent();
				$('body').append('<div id="layoutDeleteTree"><div><p>bạn có muốn Delete User này?</p><div id="DeleteTree"><span class="noDelete">No</span><span class="yesDelete">Yes</span></div></div></div>');
				$('#DeleteTree span').click( function( e ) {
					$('#layoutDeleteTree').remove();
					if( $(this).hasClass('yesDelete') ) {
						console.log('viet ham Ajax remove user co id =', parent.data('id'));
						// $.get('url', function( data ) {
						// 	if (data.deleteItem === true) {
								parent.fadeOut(800, function() {
									$(this).remove();
								});
						// 	}
						// }, 'json');
					}
					e.stopPropagation();
				});

				e.stopPropagation();
			}

			$('.removeGroup').click(removeGroup);
			function removeGroup( e ) {
				var parent = $(this).parent().parent();
				$('body').append('<div id="layoutDeleteTree"><div><p>bạn có muốn Delete Group này?</p><div id="DeleteTree"><span class="noDelete">No</span><span class="yesDelete">Yes</span></div></div></div>');
				$('#DeleteTree span').click( function( e ) {
					$('#layoutDeleteTree').remove();
					if( $(this).hasClass('yesDelete') ) {
						console.log('viet ham Ajax remove Group co id =', parent.data('id'));
						// $.get('url', function( data ) {
						// 	if (data.deleteItem === true) {
								parent.fadeOut(600, function() {
									$(this).remove();
								});
						// 	}
						// }, 'json');
					}
					e.stopPropagation();
				});

				e.stopPropagation();
			}

			$('.quyenUser span').bind('click', quyenUser);
			function quyenUser( e ) {
				var idItems = $(this).parent().parent().data('id');
				var quyen = $(this).data('quyen');
				if ( $(this).hasClass('applyQuyen') ) {
					console.log('viet ham ajax remove quyen \"', quyen, '\" cua User id = ', idItems);

					$(this).removeClass('applyQuyen');
				} else {
					console.log('viet ham ajax add quyen \"', quyen, '\" cho User id = ', idItems);

					$(this).addClass('applyQuyen');
				}

				e.stopPropagation();
			}

			$('.quyenGroup span').bind('click', quyenGroup);
			function quyenGroup( e ) {
				var idItems = $(this).parent().parent().data('id');
				var quyen = $(this).data('quyen');
				if( $(this).hasClass('applyQuyen') ) {
					console.log('viet ham ajax remove quyen \"', quyen, '\" cua Group id = ', idItems);

					$(this).removeClass('applyQuyen');
				} else {
					console.log('viet ham ajax add quyen \"', quyen, '\" cho Group id = ', idItems);

					$(this).addClass('applyQuyen');
				}
				e.stopPropagation();
			}
		}
	});

})(jQuery);