$(document).ready( function () {

	$(this).addEventHeader();

	$('#input-seach-tree').keyup( function(e) {
		var textSeach = this.value;
		if(this.value == ' ') {
			this.value = '';
		} else if (this.value !== '') {
			$('.listSeachTree').remove();
			var ul = $('<ul class="listSeachTree"></ul>');
			$('#treeBody ul').first().find('a').each( function(index, value) {
				if (value.text.indexOf(textSeach) !== -1 ) {
					ul.append($(value).parent().clone().removeClass('listTree listTreeDau').click( function() {
						$('.active-tree').removeClass('active-tree');
						$(this).children('a').addClass('active-tree');
						$('#body').html('');
						var loading = $().loading();
						$('#body').append(loading);
						setTimeout(function() {
							$.get('main-product.html', function( data ) {
								loading.remove();
								$('#content-page').html(data).addEventProduct();
								svgcheck();
							}, 'html' );
						}, 800);
						e.stopPropagation();
					}).each( function(index, item) {
						return $(item).children('ul').remove();
					}));
				}
			});
			$('#treeBody').find('ul').hide().end().append(ul);
		} else {
			$('#treeBody ul').first().find('a').each( function(index, value) {
				if ($(value).text() === $('.active-tree').text() ) {
					$(value).addClass('active-tree');
				}
			});
			$('.active-tree').parents('#treeBody ul').show();
			$('.listSeachTree').remove();
			$('#treeBody > ul').show();
		}

	});


	/* event Product
	---------------------------------------------------------------------------------------*/
	// evnet tree
	$('#treeBody li').click( function( e ) {
		if ($(this).hasClass('listTree') && $(this).hasClass('selectTree')) {
			$(this).removeClass('selectTree').children('ul').hide();
		} else if ($(this).hasClass('listTree')) {
			$(this).addClass('selectTree').children('ul').show();
		}

		console.log('li');
		return e.stopPropagation();
	});

	$('#treeBody a').click( function( e ) {
		$('.active-tree').removeClass('active-tree');
		$(this).addClass('active-tree');
		$('#body').html('');
		var loading = $().loading();
		$('#body').append(loading);
		console.log('ham Ajax View product');
		setTimeout(function() {
			$.get('main-product.html', function( data ) {
				loading.remove();
				$('#content-page').html(data).addEventProduct();
				svgcheck();
			}, 'html' );
		}, 800);
		return e.stopPropagation();
	});


	/* event new/edit/delete product */
	$('#new-product').click( function( e ) {
		$('.titleTab').remove();
		$('#body-content > div').fadeOut(200);
		$('#combobox').remove();
		var loading = $().loading();
		$('#body-content').append(loading);
		setTimeout( function() {
			$.get('newProduct.html', function(data) {
				loading.remove();
				$('#body-content').append(data).addEventCombobox({
					getUrl: 'link.get/post (newProduct)',
					addUserUrl: 'links Add User (newProduct)',
					addGroupUrl: 'links add Group (newProduct)'
				});
				svgcheck();
			}, 'html');
		}, 800);

		$('#box-header-page').append($('<div class="titleTab activeMenu">New Product</div>'));

		console.log('new product');
		e.stopPropagation();
	});

});