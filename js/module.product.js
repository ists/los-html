(function($) {

	$.fn.extend({
		treeActive: function() {
			return $('.active-tree').parent();
		},
		addEventProduct: function() {

			/* event tab-product */
			$('#tab-view span').click( function( e ) {
				$('.active').removeClass('active');
				$(this).addClass('active');
				if(this.id === 'tab-ACL') {
					$('#body-main').hide();
					$('#body-ACL').show();
				} else if (this.id === 'tab-main-UDF') {
					$('#body-ACL').hide();
					$('#body-main').show();
				}
				e.stopPropagation();
			});

			/* event new/edit/delete product */
			$('#new-product').click( function( e ) {
				$('.titleTab').remove();
				$('#body-content > div').fadeOut(200);
				$('#combobox').remove();
				var loading = $().loading();
				$('#body-content').append(loading);
				setTimeout( function() {
					$.get('newProduct.html', function(data) {
						loading.remove();
						$('#body-content').append(data).addEventCombobox({
							getUrl: 'link.get/post (newProduct)',
							addUserUrl: 'links Add User (newProduct)',
							addGroupUrl: 'links add Group (newProduct)'
						});
						svgcheck();
					}, 'html');
				}, 800);

				$('#box-header-page').append($('<div class="titleTab activeMenu">New Product</div>'));

				console.log('new product');
				e.stopPropagation();
			});


			$('#edit-product').click( function( e ) {
				var tree = document.querySelector('.active-tree');
				if (tree !== null) {
					$('.titleTab').remove();
					$('#body-content > div').fadeOut(200);
					$('#combobox').remove();
					var loading = $().loading();
					$('#body-content').append(loading);
					setTimeout( function() {
						$.get('editProduct.html', function(data) {
							loading.remove();
							$('#body-content').append(data).addEventCombobox({
								getUrl: 'link.get/post (editProduct)',
								addUserUrl: 'links Add User (editProduct)',
								addGroupUrl: 'links add Group (editProduct)'
							});
							svgcheck();
						}, 'html');
					}, 800);

					$('#box-header-page').append($('<div class="titleTab activeMenu">Edit Product</div>'));
				}

				console.log('edit product');
				e.stopPropagation();
			});

			$('#delete-product').click( function( e ) {
				var tree = document.querySelector('.active-tree');
				if (tree !== null) {
					var $this = $(this);
					$('body').append('<div id="layoutDeleteTree"><div><p>bạn có muốn Delete Tree này?</p><div id="DeleteTree"><span class="noDelete">No</span><span class="yesDelete">Yes</span></div></div></div>');

					$('#DeleteTree span').click( function( e ) {
						if( $(this).hasClass('yesDelete') ) {
							$('.titleTab').remove();
							$("#treeBody").getActiveNode().parent().remove();
							$('#body-content').html('');
							console.log('viet Layout delete product');
						}
						$('#layoutDeleteTree').remove();
						e.stopPropagation();
					});
				}
				e.stopPropagation();
			});
		},
		addEventCombobox: function() {
			var setting = arguments[0];
			var browser = $().browserName();
			console.log(setting);

			if (!(browser === 'Chrome' || browser === 'Safari' || browser === 'Opera')) {
				$(function() {
					$(".date").datepicker();
				});
			};
			
			$('select:not([name="quyenUser"],[name="quyenGroup"])').msDropdown({roundedBorder:false});
			$('.changeACL').hide();


			$('#tab-view-new-edit span').click( function() {
				$('.actived').removeClass('actived');
				$(this).addClass('actived');
				if(this.id === 'tab-ACL') {
					$('.change-Main-Udf').hide();
					$('.changeACL').show();
				} else if (this.id === 'tab-main-UDF') {
					$('.changeACL').hide();
					$('.change-Main-Udf').show();
				}
			});

			$('#exit-combobox').click( function( e ) {
				$('.titleTab').remove();
				$('#combobox').fadeOut(300).remove();
				$('#body-content > div').show();
				$('.quyenUser span').unbind('click');
				$('.quyenGroup span').unbind('click');
				e.stopPropagation();
			});

			$('#save-product').click( function( e ) {
				$('#exit-combobox').click();
				var idproduct = $("#treeBody").getActiveNode().parent().data('id');

				console.log(idproduct);

				console.log('viet ham Ajax $.get(', setting.getUrl, ')');
				$.post(setting.getUrl, function( data ) {
					// viet ham post save product
				});

				e.stopPropagation();
			});

			$('.addUser').click( function( e ) {
				var form = document.formAddUser;
				var quyen = [];
				$(form).find('input[name="quyenUser[]"]').each( function(index, item) {
					quyen.push(item.value);
				});
				if (form.userAdd.value != '' && quyen.length > 0) {
					console.log('viet ham Ajax add User');
					// $.post(setting.addUserUrl, function(data) {
						
						// neu post add user thanh cong thi them (data) tra ve vao .listUser
						// $('.listUser').append(data);
						$('.listUser').prepend($('<li data-id="'+ form.userAdd.value +'" class="border-bottom-1-2"><div class="nameUser"><span  class="icon-user"></span><a href="#">'+ form.userAdd.value +'</a></div><div class="quyenUser"><span class="border-1 applyQuyen" data-quyen="read">Read</span><span class="border-1" data-quyen="write">Write</span><span class="border-1"  data-quyen="admin">Admin</span></div><div class="deleteUser"><span class="removeUser icon-close"></span></div></li>').fadeIn(600));
					// });


					console.log('addUser- ', setting.addUserUrl);
				}
				
				console.log(quyen);
				var item = form.userAdd.options[form.userAdd.selectedIndex];
				console.log(item.value);
				console.log(item.parentNode.removeChild(item));

				$('.removeUser').unbind('click').bind('click', removeUser);
				$('.quyenUser span').unbind('click').bind('click', quyenUser);
				e.stopPropagation();
			});

			$('.addGroup').click( function( e ) {
				var form = document.formAddGroup;
				var quyen = [];
				$(form).find('input[name="quyenGroup[]"]').each( function(index, item) {
					quyen.push(item.value);
				});
				if (form.selectGroup.value != '' && quyen.length > 0) {
					console.log('Viet ham Ajax add Group');
					// $.post(setting.addGroupUrl, function( data ) {

						// neu post add Group thanh cong thi them (data) tra ve vao .listGroup
						// $('.listGroup').append(data);
						$('.listGroup').prepend($('<li data-id="'+ form.selectGroup.value +'" class="border-bottom-1-2"><div class="nameGroup"><span class="icon-users">'+ form.selectGroup.value +'</span></div><div class="quyenGroup"><span class="border-1" data-quyen="read">Read</span><span class="border-1" data-quyen="write">Write</span><span class="border-1 applyQuyen" data-quyen="admin">Admin</span></div><div class="deleteGroup"><span class="removeGroup icon-close"></span></div></li>').fadeIn(600));
					// });
					console.log('addGroup- ', setting.addGroupUrl)
				}

				$('.removeGroup').unbind('click').bind('click', removeGroup);
				$('.quyenGroup span').unbind('click').bind('click', quyenGroup);
				e.stopPropagation();
			});

			$('.removeUser').click(removeUser);
			function removeUser( e ) {
				var parent = $(this).parent().parent();
				$('body').append('<div id="layoutDeleteTree"><div><p>bạn có muốn Delete User này?</p><div id="DeleteTree"><span class="noDelete">No</span><span class="yesDelete">Yes</span></div></div></div>');
				$('#DeleteTree span').click( function( e ) {
					$('#layoutDeleteTree').remove();
					if( $(this).hasClass('yesDelete') ) {
						console.log('viet ham Ajax remove user co id =', parent.data('id'));
						// $.get('url', function( data ) {
						// 	if (data.deleteItem === true) {
								parent.fadeOut(800, function() {
									$(this).remove();
								});
						// 	}
						// }, 'json');
					}
					e.stopPropagation();
				});
	console.log(parent);
				e.stopPropagation();
			}

			$('.removeGroup').click(removeGroup);
			function removeGroup( e ) {
				var parent = $(this).parent().parent();
				$('body').append('<div id="layoutDeleteTree"><div><p>bạn có muốn Delete Group này?</p><div id="DeleteTree"><span class="noDelete">No</span><span class="yesDelete">Yes</span></div></div></div>');
				$('#DeleteTree span').click( function( e ) {
					$('#layoutDeleteTree').remove();
					if( $(this).hasClass('yesDelete') ) {
						console.log('viet ham Ajax remove Group co id =', parent.data('id'));
						// $.get('url', function( data ) {
						// 	if (data.deleteItem === true) {
								parent.fadeOut(600, function() {
									$(this).remove();
								});
						// 	}
						// }, 'json');
					}
					e.stopPropagation();
				});

				e.stopPropagation();
			}

			$('.quyenUser span').bind('click', quyenUser);
			function quyenUser( e ) {
				var idItems = $(this).parent().parent().data('id');
				var quyen = $(this).data('quyen');
				if ( $(this).hasClass('applyQuyen') ) {
					console.log('viet ham ajax remove quyen \"', quyen, '\" cua User id = ', idItems);

					$(this).removeClass('applyQuyen');
				} else {
					console.log('viet ham ajax add quyen \"', quyen, '\" cho User id = ', idItems);

					$(this).addClass('applyQuyen');
				}

				e.stopPropagation();
			}

			$('.quyenGroup span').bind('click', quyenGroup);
			function quyenGroup( e ) {
				var idItems = $(this).parent().parent().data('id');
				var quyen = $(this).data('quyen');
				if( $(this).hasClass('applyQuyen') ) {
					console.log('viet ham ajax remove quyen \"', quyen, '\" cua Group id = ', idItems);

					$(this).removeClass('applyQuyen');
				} else {
					console.log('viet ham ajax add quyen \"', quyen, '\" cho Group id = ', idItems);

					$(this).addClass('applyQuyen');
				}
				e.stopPropagation();
			}

			return false;
		}
	});

})(jQuery);