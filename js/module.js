(function($) {

	$.fn.extend({
		hideDropdown: function() {
			$("* [class*='dropdown-menu']").hide();
			return this;
		},
		addEventHeader: function() {
			/* event Header 
			---------------------------------------------------------------------------------------*/
			$('#historyWork .icon-clock').click( function( e ) {
				$(this).hideDropdown().next().show();
				return false;
			});

			$('#Notification .icon-bell').click( function( e ) {
				$(this).hideDropdown().next().show();
				return false;
			});

			$('#accout .icons-settings').click( function( e ) {
				$(this).hideDropdown().next().show();
				return false;
			});

			$(document).click( function() {
				$().hideDropdown();
				console.log('hji');
			});

		},
		loading: function() {
			return $('<div class="loading"><img src="images/ajax-loader.gif"></div>');
		},
		browserName: function() {
			var Browser = navigator.userAgent;
			if (Browser.indexOf('MSIE') >= 0){
				Browser = 'MSIE';
			}
			else if (Browser.indexOf('Firefox') >= 0){
				Browser = 'Firefox';
			}
			else if (Browser.indexOf('Chrome') >= 0){
				Browser = 'Chrome';
			}
			else if (Browser.indexOf('Safari') >= 0){
				Browser = 'Safari';
			}
			else if (Browser.indexOf('Opera') >= 0){
				Browser = 'Opera';
			}
			else{
				Browser = 'UNKNOWN';
			}
			return Browser;
		},
		getActiveNode: function() {
			return $(this).find('.active-tree');
		},
		test: function() {
			console.log('test');

			return this;
		}
	});

})(jQuery);