


(function($) {
	$.fn.extend({
		eventViewDuyetItem: function() {
			var obj = arguments[0];

			if(obj.id !== undefined) {
				$('.exitViewDuyet').click( function( e ) {
					$('#viewDuyetItem').remove();
				});

				$('#viewDuyetItem .bottomAppreve').click( function() {
					// viet ham Ajax appreve item
					console.log('viet ham Ajax appreve item voi id = ', obj.id);

					/*$.get('url', function(data) {

					});*/
					// xoa item sau khi appreve
					obj.item.parent().parent().fadeOut(800, function() {
						$(this).remove();
					});
					$('.exitViewDuyet').click();
				});

				$('#viewDuyetItem .bottomReject').click( function() {
					// viet ham Ajax rejects item 
					console.log('viet ham Ajax rejects item voi id = ', obj.id);

					/*$.get('url', function(data) {

					});*/
					// xoa item sau khi Reject
					obj.item.parent().parent().fadeOut('slow', function() {
						$(this).remove();
					});
					$('.exitViewDuyet').click();
				});

				$('#tab-view-duyetItem span').click( function(e) {
					$('#tab-view-duyetItem .active').removeClass('active');
					$(this).addClass('active');

					if ($(this).attr('id') === 'tab-ACL') {
						$('#main-udf-duyetItem').hide();
						$('#acl-duyetItem').show();
					} else if ($(this).attr('id') === 'tab-main-UDF') {
						$('#acl-duyetItem').hide();
						$('#main-udf-duyetItem').show();
					}
				});
			}
		},
		eventListItem: function() {
			$('.truongActive a').click( function(e) {
				var $this = $(this);
				var idItem = $(this).parent().parent().data('id');

				// a.viewProduct
				if( $this.hasClass('viewProduct') ) {
					// get send id
					$.get('duyetItem.html', function( data ) {
						$('body').append(data).eventViewDuyetItem({id: idItem, item: $this});
						svgcheck();
					}, 'html');
				}

				return false;
			});
		
			$('.listDuyetItem li:not(.titleTruong)').click( function( e ){
				$('.Active').removeClass('Active');
				$(this).addClass('Active');
				e.stopPropagation();
			});
		}
	});
})(jQuery);