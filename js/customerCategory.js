$(document).ready( function () {

	$(this).addEventHeader();

		$('#input-seach-tree').keyup( function(e) {
		var textSeach = this.value;
		if(this.value == ' ') {
			this.value = '';
		} else if (this.value !== '') {
			$('.listSeachTree').remove();
			var ul = $('<ul class="listSeachTree"></ul>');
			$('#treeBody ul').first().find('a').each( function(index, value) {
				if (value.text.indexOf(textSeach) !== -1 ) {
					ul.append($(value).parent().clone().removeClass('listTree listTreeDau').click( function() {
						$('.active-tree').removeClass('active-tree');
						$(this).children('a').addClass('active-tree');
						$('#body').html('');
						var loading = $().loading();
						$('#body').append(loading);
						setTimeout(function() {
							$.get('customerCategoryMain.html', function( data ) {
								loading.remove();
								$('#content-page').html(data).addEventProduct();
								svgcheck();
							}, 'html' );
						}, 800);
						e.stopPropagation();
					}).each( function(index, item) {
						return $(item).children('ul').remove();
					}));
				}
			});
			$('#treeBody').find('ul').hide().end().append(ul);
		} else {
			$('#treeBody ul').first().find('a').each( function(index, value) {
				if ($(value).text() === $('.active-tree').text() ) {
					$(value).addClass('active-tree');
				}
			});
			$('.active-tree').parents('#treeBody ul').show();
			$('.listSeachTree').remove();
			$('#treeBody > ul').show();
		}

	});


	/* event Customer Category
	---------------------------------------------------------------------------------------*/
	// evnet tree
	$('#treeBody li').click( function( e ) {
		if ($(this).hasClass('listTree') && $(this).hasClass('selectTree')) {
			$(this).removeClass('selectTree').children('ul').hide();
		} else if ($(this).hasClass('listTree')) {
			$(this).addClass('selectTree').children('ul').show();
		}

		console.log('li');
		return false;
	});


	$('#treeBody a').click( function( e ) {
		$('.active-tree').removeClass('active-tree');
		$(this).addClass('active-tree');
		$('#body').html('');
		var loading = $().loading();
		$('#body').append(loading);
		console.log('ham Ajax View product');
		setTimeout(function() {
			$.get('mainTest.html', function( data ) {
				loading.remove();
				$('#content-page').html(data).addEventProduct();
				svgcheck();
			}, 'html' );
		}, 800);
		return e.stopPropagation();
	});

	$('#new-customerCategory').click( function( e ) {
		$('.titleTab').remove();
		$('#body-content > div').fadeOut(200);
		$('#combobox').remove();
		var loading = $().loading();
		$('#body-content').append(loading);
		setTimeout( function() {
			$.get('customerCategory-new.html', function(data) {
				loading.remove();
				$('#body-content').append(data).addEventCombobox({
					getUrl: 'link.get/post (newProduct)',
					addUserUrl: 'links Add User (newProduct)',
					addGroupUrl: 'links add Group (newProduct)'
				});
				svgcheck();
			}, 'html');
		}, 800);

		$('#box-header-page').append($('<div class="titleTab activeMenu">New Product</div>'));

		console.log('new-customerCategory');
		e.stopPropagation();
	});

	$('#edit-customerCategory').click( function(e) {
		var tree = document.querySelector('.active-tree');
		if (tree !== null) {
			$('.titleTab').remove();
			$('#body-customer-category > div').fadeOut(200);
			$('#combobox').remove();
			var loading = $().loading();
			$('#content-customer-category').append(loading);
			setTimeout( function() {

				$.get('customerCategory-edit.html', function( data ) {
					loading.remove();
					$("#body-customer-category").append(data).addEventComboBox({
						getUrl: 'link.get/post (new Customer Category)',
						addUserUrl: 'links Add User (new Customer Category)',
						addGroupUrl: 'links add Group (new Customer Category)'
					});
					svgcheck();
				}, 'html');
		}, 800);

			$('#box-header-customer-category').append($('<div class="titleTab activeMenu">Edit Customer Category</div>'));

			console.log('edit-customer-category');
		}
		e.stopPropagation();
	});

	$('#delete-customer-category').click( function(e) {
		var tree = document.querySelector('.active-tree');
		if (tree !== null) {
			var $this = $(this);
			$('body').append('<div id="layoutDeleteTree"><div><p>bạn có muốn Delete Tree này?</p><div id="DeleteTree"><span class="noDelete">No</span><span class="yesDelete">Yes</span></div></div></div>');

			$('#DeleteTree span').click( function( e ) {
				if( $(this).hasClass('yesDelete') ) {
					$('.titleTab').remove();
					$("#treeBody").getActiveNode().parent().remove();
					$('#body-customer-category').html('');
					console.log('viet Layout delete customer category');
				}
				$('#layoutDeleteTree').remove();
				e.stopPropagation();
			});
		}
		e.stopPropagation();
	});

});